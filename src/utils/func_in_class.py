class funcInClass:
    def __init__(self, message):
        self.something = "abc"
        self.message = message
    
    def readFile(self, fileName):
        file = open(fileName, 'r')
        linesInFile = file.readlines()
        for line in linesInFile:
            line.strip()
    
    def testFunc(self):
        print (self.message)        