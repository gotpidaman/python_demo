import time
import os

from utils import func_utils #@UnresolvedImport
from utils import func_in_class #@UnresolvedImport

while True:
    entries = os.listdir(os.environ['ROOT_FILE'])
    
    if len(entries) < 1:
        print ("don't have file")
        time.sleep(2)
        continue
    else:
        func_utils.printMessage("have fileeee")
        
        funcClass = func_in_class.funcInClass("class test")
        funcClass.testFunc()

    time.sleep(2)